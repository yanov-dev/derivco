﻿// ----------------------------------------------------------------------------
// <copyright file="SolutionCalculator.cs" company="Derivco">
//   Copyright (C) Derivco 2017 All rights reserved
// </copyright>
// ----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Derivco.FullStack.Assignment.Extensions;

namespace Derivco.FullStack.Assignment
{
    public class SolutionCalculator : ISolutionCalculator
    {
        #region ISolutionCalculator implementation

        public Solution Calculate(IList<Rectangle> inputRectangles)
        {
            return new Solution
            {
                InputRectangles = inputRectangles,
                OutputRectangles = CalculateSolution(inputRectangles)
            };
        }

        #endregion

        private bool Iterate(List<RectangleArea> areas, List<Rectangle> rects, List<Rectangle> output)
        {
            if (CanPlaceAllRectangles(areas, rects))
            {
                foreach (var area in areas)
                foreach (var rect in area.GetAllInnerRectangles())
                    output.Add(rect);

                return true;
            }

            var biggestArea = areas.First(e => e.Width == areas.Max(a => a.Width));
            var biggestRect = rects.First(e => e.GetMaxSide() == rects.Max(r => r.GetMaxSide()));

            biggestRect.PlaceHorizontally();
            biggestRect.Bottom = biggestArea.Botton;
            biggestRect.Left = biggestArea.Left;

            areas.Add(new RectangleArea
            {
                Width = biggestRect.GetMaxSide(),
                Left = biggestArea.Left,
                Botton = biggestArea.Botton + biggestRect.GetMinSide()
            });

            biggestArea.Width -= biggestRect.GetMaxSide();
            biggestArea.Left += biggestRect.GetMaxSide();

            rects.Remove(biggestRect);
            output.Add(biggestRect);

            return false;
        }

        private bool CanPlaceAllRectangles(List<RectangleArea> areas, List<Rectangle> rects)
        {
            if (rects.Count == 0)
                return true;

            foreach (var area in areas)
                area.Clear();

            foreach (var rect in rects.OrderByDescending(e => e.GetMinSide()))
                if (!CanPlaceRectangleInAnyArea(areas, rect))
                    return false;

            return true;
        }

        private bool CanPlaceRectangleInAnyArea(List<RectangleArea> areas, Rectangle rect)
        {
            foreach (var area in areas)
                if (area.TryImport(rect))
                    return true;

            return false;
        }

        private IList<Rectangle> CalculateSolution(IList<Rectangle> inputRectangles)
        {
            if (inputRectangles.Count == 0)
                throw new ArgumentException("collection mustn't be empty");

            var clonedRectangles = new List<Rectangle>();
            foreach (var r in inputRectangles)
                clonedRectangles.Add(r.Clone());

            var rect = clonedRectangles.GetLongestRectangle();

            clonedRectangles.Remove(rect);

            rect.PlaceHorizontally();

            rect.Left = 0;
            rect.Bottom = 0;

            var areas = new List<RectangleArea>
            {
                new RectangleArea
                {
                    Width = rect.GetMaxSide(),
                    Botton = rect.GetMinSide()
                }
            };

            var result = new List<Rectangle> {rect};

            const int MAX_ITERATIONS = 50;

            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                if (Iterate(areas, clonedRectangles, result))
                    return result;
            }

            throw new Exception("Something went wrong. the algorithm is probably broken");
        }

        private class RectangleArea
        {
            private readonly List<Rectangle> _rects;

            public IEnumerable<Rectangle> GetAllInnerRectangles()
            {
                return _rects;
            }

            public RectangleArea()
            {
                _rects = new List<Rectangle>();
            }

            public int Botton { get; set; }

            public int Left { get; set; }

            public int Width { get; set; }

            private int GetFreeWidth()
            {
                return Width - _rects.Sum(e => e.GetMinSide());
            }

            public void Clear()
            {
                _rects.Clear();
            }

            public bool TryImport(Rectangle rect)
            {
                if (rect.GetMinSide() > GetFreeWidth()) 
                    return false;
                
                rect.Bottom = Botton;
                rect.Left = Left + GetFreeWidth() - rect.GetMinSide();
                rect.PlaceVertically();

                _rects.Add(rect);
                return true;
            }
        }
    }
}