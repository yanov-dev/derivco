﻿// ----------------------------------------------------------------------------
// <copyright file="RectangleExtensions.cs" company="Derivco">
//   Copyright (C) Derivco 2018 All rights reserved
// </copyright>
// ----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;

namespace Derivco.FullStack.Assignment.Extensions
{
    public static class RectangleExtensions
    {
        public static int GetMinSide(this Rectangle rectangle)
        {
            return Math.Min(rectangle.Width, rectangle.Height);
        }

        public static int GetMaxSide(this Rectangle rectangle)
        {
            return Math.Max(rectangle.Width, rectangle.Height);
        }

        public static void PlaceHorizontally(this Rectangle rectangle)
        {
            if (rectangle.Height > rectangle.Width)
            {
                var c = rectangle.Height;
                rectangle.Height = rectangle.Width;
                rectangle.Width = c;
            }
        }

        public static Rectangle GetLongestRectangle(this IList<Rectangle> collection)
        {
            if (collection.Count == 0)
                throw new ArgumentException("collection mustn't be empty");

            var rect = collection[0];

            for (int i = 1; i < collection.Count; i++)
            {
                var item = collection[i];
                if (item.GetMaxSide() > rect.GetMaxSide())
                    rect = item;
            }
            
            return rect;
        }

        public static void PlaceVertically(this Rectangle rectangle)
        {
            if (rectangle.Width > rectangle.Height)
            {
                var c = rectangle.Height;
                rectangle.Height = rectangle.Width;
                rectangle.Width = c;
            }
        }
    }
}